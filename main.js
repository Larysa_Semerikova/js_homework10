const menu = document.querySelector(".tabs");

menu.addEventListener("click", (event)=>{
    let target = event.target.dataset.item;

   document.querySelector(".active-tab").classList.remove("active-tab");

   document.querySelector(".active-text").classList.remove("active-text");

   document.querySelector(`[data-text = ${target}]`).classList.add("active-text");

   event.target.classList.add("active-tab")

})